/*global
sosParse, tabs, render
*/

var dialogsToClose = [];

function closeDialogs(){
  dialogsToClose.forEach(function(diag){
    $(diag).dialog("close");
  });
  dialogsToClose = [];
}

if(window.location.href === "https://live.sosinventory.com/Item"){
  $("body").append('<div style="max-width: 600px;" id="plugin-footer"></div>');
  
  // monitor the page for content changes
  var reloadingVisible = false;
  setInterval(function(){
    var isVisible = $("#list_processing").css("visibility") === "visible";
    
    // redraw buttons if the table just updated
    if(isVisible === false && reloadingVisible === true){
      fillStockIcons();
    }
    
    reloadingVisible = isVisible;
  }, 10);
  
  function fillStockIcons(){
    closeDialogs();
    // Get the index of the "In Stock" and "Name" columns
    var nameIndex = null;
    var stockIndex = null;
    $("#list_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead > tr > th").each(function(index, el){
      if(el.innerText === "Name")
        nameIndex = index;
      if(el.innerText === "In Stock")
        stockIndex = index;
    });

    var imgId = 0;

    // Append the location icon in each Stock cell
    $("#list > tbody > tr").each(function(index, el){
      imgId ++;
      stockEl = $(el).find("td")[stockIndex];
      $(stockEl).prop('title', "This location query functionality is provided by the SOS Expanded BOM Chrome plugin (not part of regular SOS)");
      $(stockEl).append('&nbsp;&nbsp;<img id="whereImg' + imgId + '" src="' + chrome.extension.getURL('where.png') + '" height="16" width="16">');
      
      // Get the cell name
      var name = $(el).find("td")[nameIndex].innerText;
      
      // Assign the stock location query callback
      $(stockEl).click(function(){
        showLocations(name);
      });
      
      // Set the cursor to show it can be clicked
      $(stockEl).css('cursor', 'pointer');
    });
  }
}

if(window.location.href === "https://live.sosinventory.com/ReportV2/ExpandedBOM"){
  $(".page-content").append("<div id='bom-plugin'></div>");
  var selector = "#bom-plugin";
  var updatedText = "";
  
  $(".report-table-container").prepend('<button id="toggle-sos-table"><span id="show-hide-table-span">Show</span> original table</button>');
  $('#toggle-sos-table').click(function(){
    $("#report-table").toggle();
    if($("#report-table").is(":visible")){
      $("#show-hide-table-span").text("Hide");
    }else{
      $("#show-hide-table-span").text("Show");
    }
  });
  $('#toggle-sos-table').hide();

  setInterval(function(){
    var newText = $("#report-generated-text").text();
    if(newText !== updatedText){
      updatedText = newText;
      update();
      $("#report-table").hide();
      $('#toggle-sos-table').show();
    }
  }, 300);

  setTimeout(update,0);

  function update(){
    closeDialogs();
    
    $(selector).empty();
    
    $(selector).append('<div id="calculating">Please wait... Calculating<br/><br/><div class="loader"></div></div>');
    $(selector).append('<div id="plugin-header"></div>');
    $(selector).append('<div id="plugin-tabs"></div>');
    $(selector).append('<div style="max-width: 600px;" id="plugin-footer"></div>');

    // Create the tabs
    tabs.init("#plugin-tabs");
    tabs.add("design", "Graph");
    tabs.add("inventory", "Table", function(){
      setTimeout(function(){
        $("#tabulator-inventory").tabulator("redraw");
      }, 10);
    });

    $( window ).resize(function() {
      $("#tabulator-inventory").tabulator("redraw");
    });

    $("#plugin-header").append('<br/><br/><strong>Visual expanded BOM plugin is ready. Select an item, then click on "Display Report" to generate a table.</strong>');

    // Create the HTML containers
    $("#calculating").show();

    // Do a parse of the SOS inventory table
    sosParse.parse();

    // Analyze the data
    var analysis = report(sosParse);

    // Use the inventory from the analysis (to handle the special CURRENT_BATCHES case)
    sosParse.inventory = analysis.inventory;

    // No need to process an empty page
    if(analysis.buildResults === undefined){

    }else{
      // Render the content
      render.topPart(sosParse, analysis);
      render.inventory(sosParse, analysis);
      //render.sosTable();
      render.designTree(sosParse, analysis);
      render.footer(sosParse, analysis);
    }
    $("#calculating").hide();
  }
}

// This function just makes an ajax call to SOS inventory servers, to retrieve the stock location for the given sku. It then shows a popup with the result.
function showLocations(sku){
  function interpretResult(msg){
    if(msg && msg.status === "OK"){
      // parse the result
      header = []; 
      parsedHeader = $.parseHTML(msg.thead);
      $(parsedHeader).find("th").each(function(index, el){
         header.push(el.innerText);
      });
      
      body = []; 
      parsedBody = $.parseHTML(msg.tbody);
      $(parsedBody).find("td").each(function(index, el){
         body.push(el.innerText);
      });
      
      // hide all columns that are zero and generate the html table
      var arr = [];
      header.forEach(function(el, index){
        if((body[index] !== "0" || el === "Total") && el !== "Description" && body[index]){
          var el = el;
          var bod = body[index];
          
          if(el === "Name" || el === "Total"){
            el = "<strong>" + el + "</strong>";
            bod = "<strong>" + bod + "</strong>";
          }
          
          arr.push([el, bod]);
        }
      });
      var strTable = "";
      for(var i = 0; i < arr.length; i++) {
        strTable += "<tr>"
        for(var j = 0; j < arr[i].length; j++) {
          strTable += "<td>";
          strTable += "&nbsp;&nbsp;" + arr[i][j] + "&nbsp;&nbsp;";
          strTable += "</td>";
        }
        strTable += "</tr>";
      }
      

      //Calculate the dialogID
      showLocations.dialog = showLocations.dialog || 1;
      showLocations.dialog++;

      //Create the dialog
      var diagID = "dialoglocations" + showLocations.dialog
      $("#plugin-footer").append('<div id="' + diagID + '" title="&nbsp;Stock status for ' + sku + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + '"></div>');
      $("#"+diagID).html(strTable);
      $("#"+diagID).dialog({
       height:'auto',
       width:'auto',
       minHeight: 'auto',
       resizable: false,
       beforeClose: function(){
         $( "#"+diagID ).parent().hide(); //prevents whole page from scrolling when dialog is closed
         return false;
       },
       open: function(){
       },
       dialogClass: "custom-dialog-1"
      });
      $("#"+diagID).css("maxHeight", 700);
      dialogsToClose.push("#"+diagID);

      //Fix a weird JqueryUI bug (maybe a CSS conflict somewhere with SOS)
      var label = $("div.ui-dialog-titlebar.ui-corner-all.ui-widget-header.ui-helper-clearfix.ui-draggable-handle > button").html("");
    }else{
      alert("error: " + JSON.stringify(msg));
    }
  }
  
  
  if(!sku){
    console.log("No sku was specified");
    return;
  }
  
  // get the date as a string in the correct locale
  /*var language;
  if (window.navigator.languages) {
    language = window.navigator.languages[0];
  } else {
    language = window.navigator.userLanguage || window.navigator.language;
  }*/
  var d = new Date();
  var todayStr = d.toUTCString();
  
  // go to the "inventory stock status" report to learn the correct format, it may change when they update the software.
  $.ajax({
    type: "POST",
    url: "/ReportV2/GetReportHtml",
    data: JSON.stringify({
      reportType: "InventoryStockStatus",
      settings: "vendorname=|customername=undefined|itemname=undefined|date1=" + todayStr + "|date1macro=|date2=undefined|date2macro=undefined|exportformat=xlsx|namecontains=" + sku + "|namecontains2=undefined|location=0|category=null|vendor=0|customer=undefined|suppressifempty=0|groupby=None|valuecalculation=On Hand|header=|footer=|showfromto=0|showdeleted=0|showarchived=0|includepos=0|includewos=0|includermas=0|includerentals=0|includenulls=0|showfullonly=0|showpicked=0|useavailable=0|addtoreorderlist=0|ordertype=undefined|item=undefined|txntype=undefined|salesrep=undefined|showlots=0|showserials=0|showbins=0|user=undefined|quantity=undefined|pricetier=undefined|channelid=undefined|classid=undefined|departmentid=undefined|customertypeid=undefined|highlightreorders=0|",
      fields: "Name,Description,"
    }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: interpretResult,
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      console.log("ERROR:");
      console.log(errorThrown);
      alert(JSON.stringify(errorThrown));
    }
  });
}