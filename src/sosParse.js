// ******************** GETS DATA FROM SOS *********************

var sosParse = {};

// Parses the SOS inventory report page
sosParse.parse = function parse(){
  var SOSArray = [];
  
  sosParse.originalArray = [];
  sosParse.design = {};
  sosParse.parents = {};
  sosParse.inventory = {};
  sosParse.supplier = {};
  sosParse.description = {};
  sosParse.notes = {};
  sosParse.onPO = {};
  sosParse.negativeInventory = false;
  sosParse._parsing = [];
  sosParse._itemParsed = {};

  $("#report-table tr").each(function() {
    var arrayOfThisRow = [];
    var tableData = $(this).find('th');
    tableData.each(function() { arrayOfThisRow.push($(this).text()); });
    tableData = $(this).find('td');
    tableData.each(function() { arrayOfThisRow.push($(this).text()); });
    SOSArray.push(arrayOfThisRow);
  });

  sosParse.buildQty = Number($("#Quantity").val());
  sosParse.rootName = $(".item-name").val();

  //Example array
  /*SOSArray = [
      ["Item","Description","Supplier","On Hand","On PO","Used In Build","Cost Each","Total","Notes"],
      ["Box","Box of design","Uline",6,0,1,"Cost Each","Total","Notes"],
      ["Tape","Regular tape","Uline",50,0,10,"Cost Each","Total","Notes"],
      ["PCB Tested","Tested Circuit board assembled","Internal job",2,0,1,"Cost Each","Total","Notes"],
      ["     Zip","Basic ziplocking bag","Uline",20,0,1,"Cost Each","Total","Notes"],
      ["     PCB","Not tested PCB","7PCB",8,0,1,"Cost Each","Total","Notes"],
      ["Hardware","Hardware bag yeah","Internal assembly",3,0,2,"Cost Each","Total","Notes"],
      ["     Zip","Basic ziplocking bag","Uline",20,0,6,"Cost Each","Total","Notes"],
      ["     Screws","Screws are important","McMaster",7,0,4,"Cost Each","Total","Notes"]
  ];*/
  //var buildQty = 1;
  
  // Important note: SOS inventory allows BOM that contain multiple times the same sku. So for example, item B can be made of: 3 x D + 4 x F + 4 x D. So in reality, it should become 7 x D + 4 x F.

  // Gives the depth of the element (based on the fact that SOS adds 5 spaces for each level). 0 means first level.
  function getDepth(name){
      var depth = name.search(/\S|$/)/5;
      if(!Number.isInteger(depth)){
          alert("Error: depth not a multiple of 5 spaces");
          return 0;
      }
      return depth;
  }

  //Get the column data from the given index
  function getData(name, rowIndex, messageIfNotFound){
      var columnIndex = -1;
      SOSArray[0].forEach(function(val, index, array){
         if(val===name){
             columnIndex = index;
         } 
      });
      if(columnIndex === -1){
         return null;
      }
      return SOSArray[rowIndex][columnIndex];
  }
  
  sosParse.inventory = {};
  sosParse.inventory[sosParse.rootName] = 0;
  sosParse.description[sosParse.rootName] = "Top level assembly";
  sosParse.supplier[sosParse.rootName] = "";
  sosParse.onPO[sosParse.rootName] = 0;

  //Iterate in the whole table to build the data in the proper format for analysis after
  var lastIndexOfDepth = [0];
  var poWarned = false;
  SOSArray.forEach(function(val, index, array){
      // parse the original data
      var item = getData("Component", index);
    
      if(item === "Component" || item === "Total")
          return;
    
      var qtyInBuild = Number(getData("Used In Build", index).replace(",", "")); //remove commas (1,000.00)
      var qtyInStock = Number(getData("On Hand", index).replace(",", ""));
      var qtyInPO = getData("On PO", index)
      if(qtyInPO !== null){
        qtyInPO = Number(qtyInPO.replace(",", ""));
      }else{
        qtyInPO = 0;
        if(!poWarned){
          poWarned = true;
          alert("The 'On PO' column is missing. Assuming 0 for all PO quantities.");
        }
      }
      
      var itemDescription = getData("Description", index);
      var notes = getData("Notes", index);
      var itemSupplier = getData("Vendor", index);
      var depth = getDepth(item);
      item = item.trim();
      lastIndexOfDepth[depth] = index;

      // save the item data
      sosParse.inventory[item] = qtyInStock;
      sosParse.description[item] = itemDescription;
      sosParse.supplier[item] = itemSupplier;
      sosParse.onPO[item] = qtyInPO;
    
      // if item name has "NON-INVENTORY" than quantity is considered infinite
      if(itemDescription.includes("NON-INVENTORY")){
        sosParse.inventory[item] = Infinity;
      }

      // check negative inventory
      if(qtyInStock < 0){
        sosParse.negativeInventory = true;
      }
    
      // an item is parsed if all its children were scanned once
      sosParse._parsing.push({
        depth: depth,
        item: item
      });
      var newParse = [];
      sosParse._parsing.forEach(function(parsing, index){
        if(parsing.depth >= depth && parsing.item !== item){
          sosParse._itemParsed[parsing.item] = true;
        }else{
          newParse.push(parsing);
        }
      });
      sosParse._parsing = newParse;
    

      // get the parent item
      var parent = sosParse.rootName;
      if(depth>0){
          parent = getData("Component", lastIndexOfDepth[depth-1]).trim();
      }
      
      // save the assembly notes. Note that notes are specific to the BOM of the parent part, and not the SKU.
      if(sosParse.notes[item] === undefined){
          sosParse.notes[item] = {};
      }
      sosParse.notes[item][parent] = notes;

      // ensure the entry exists in the design
      if(sosParse.design[parent] === undefined){
          sosParse.design[parent] = {};
      }

      // calculate the quantity used directly in the parent node (not total like SOS gives us)
      if(depth === 0){
          qtyInParent = qtyInBuild / sosParse.buildQty;
      }else{
          var parentQtyInBuild = getData("Used In Build", lastIndexOfDepth[depth-1]).replace(",", "");
          qtyInParent = qtyInBuild / parentQtyInBuild;
      }
      if(isNaN(qtyInParent)){
          qtyInParent = 0; //in some cases, it is impossible to know the build quantity if the parent is also zero, because of the way SOS inventory presents it to us.
      }
      if(sosParse.design[parent][item] === undefined){
        sosParse.design[parent][item] = 0;
      }
      if(!sosParse._itemParsed[parent]){
        sosParse.design[parent][item] += qtyInParent; 
      }
    
      // save the parent list
      if(sosParse.parents[item] === undefined){
        sosParse.parents[item] = {};
      }
      sosParse.parents[item][parent] = qtyInParent;
  });
  
  sosParse.originalArray = SOSArray;
  
  if(sosParse.negativeInventory){
      alert("Some items have negative quantities. Build reports will be incorrect. It should be fixed immediately!");
  }
  
  console.log(sosParse);
}