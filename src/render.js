// *************** RENDER FUNCTIONS *********************
// Handles displaying to the DOM

var render = {};

render.topPart = function(data, analysis){
}

render.designTree = function(data, analysis){
  tabs.content('design','<div><button id="collapse-all">Collapse All</button> <button id="expand-all">Expand All</button></div><br/>');
  tabs.append("design", '<div id="design-tree"></div>');

  // render the tree
  var canvas = "#design-tree";
  var width = $(canvas).width() - 43; //Adjusted because of margins.
  var tree = treeNodes(canvas, data.rootName, data.buildQty, data, analysis, {"width": width, "height": 800});
  
  $("#collapse-all").click(function(){
      tree.collapseAll();
      event.preventDefault();
  });
  $("#expand-all").click(function(){
      tree.expandAll();
      event.preventDefault();
  });
}

render.sosTable = function(){
  /*tabs.content("sosTable", "This is the original table as given by SOS inventory without Dominic's extension. All the extension data is interpreted from this table. You can also use it to see the build cost.<br/><br/>");
  
  // Copy the SOS table into one of the tabs instead
  var originalTable = $("#ctl00_body_pnlMain > table > tbody > tr:nth-child(7)").html();
  tabs.append("sosTable", originalTable);
  
  // Hide unneeded text
  $("#tabsosTable > td.form-label").hide();*/
}

render.inventory = function(data, analysis){
  if(!data){
    //only redraw
    $("#tabulator-inventory").tabulator("redraw");
    return;
  }
  
  // Define the table location
  // See the report.js for a Google Doc on definitions
  tabs.content('inventory','<div id="download-csv"><button id="download-csv">Download CSV</button></div><br/>');
  tabs.append('inventory','<div id="tabulator-inventory"></div>');

  //trigger download of data.csv file
  $("#download-csv").click(function(){
      $("#tabulator-inventory").tabulator("download", "csv", "data.csv");
      event.preventDefault();
  });
  
  function formatBuildStatus(cell, formatterParams){
      //cell - the cell component
      //formatterParams - parameters set for the column
      var html = '<span class="dot-';
    
      switch(cell.getValue()){
        case "Missing":
          html += 'red';               
          break;
        case "Ready to build":
          html += 'green';
          break;
        case "Can build soon":
          html += 'yellow';
          break;
        case "Non-inventory":
          html += 'grey';
          break;
        default:
          html += 'white';
          break;
      }
    
      html += '"></span> ' + cell.getValue();

      return html;
  }

  // Convert the data
  var tabledata = [];
  var id = 1;
  $.each( data.inventory, function( sku, inventory ) {
    var neededPO = '';
    if(analysis.neededPO[sku] > 0){
      neededPO = analysis.neededPO[sku];
    }
    var onPO = '';
    if(data.onPO[sku]!==0){
      onPO = data.onPO[sku];
    }
    var canBuildNow = '';
    if(analysis.canBuildNow[sku] !==0){
      canBuildNow = analysis.canBuildNow[sku];
    }
    var canBuildSoon = '';
    if(analysis.buildResultsPO[sku].buildQty !==0){
      canBuildSoon = analysis.buildResultsPO[sku].buildQty;
    }
    
    var notes = "";
    $.each(data.notes[sku], function(parent,note){
      if(note!=="")
        notes += "For " + parent + ": " + note + "\n";
    });
    
    tabledata.push({
      id: id,
      inventory: Math.round10(inventory, -4),
      sku: sku,
      onPO: onPO,
      bom: Math.round10(analysis.buildList[sku], -4),
      supplier: data.supplier[sku],
      description: data.description[sku],
      notes: notes,
      buildStatus: analysis.buildStatus[sku],
      neededPO: neededPO,
      canBuildNow: canBuildNow,
      canBuildSoon: canBuildSoon
    });
    id++;
  });
  
  //Generate design icon
  var treeIcon = function(cell, formatterParams){ //plain text value
      return '<img src="' + chrome.extension.getURL('nodes.png') + '" height="32" width="32">';
  };
  var whereIcon = function(cell, formatterParams){ //plain text value
      stock = cell.getRow().getData().inventory;
    
      if(!isFinite(stock)){
        return "Non-inventory";
      }
    
      return stock + '&nbsp;<img src="' + chrome.extension.getURL('where.png') + '" height="16" width="16">';
  };
  
  //Show the location report
  function showLocationsWrapper(e, cell){
    var sku = cell.getRow().getData().sku;
    showLocations(sku);
  }
  
  //Show the paths for a given item
  function showPaths(e, cell){
    var sku = cell.getRow().getData().sku;
    var usedPaths = analysis.usedBy[sku];
    var shortName = cell.getRow().getData().description;
    if(shortName.length>30){
        shortName = shortName.substring(0,27) + "...";
    }
    
    var text = "<strong>Needed in:</strong><br/>";
    
    usedPaths.forEach(function(path){
      var paths = [];
      path.forEach(function(node){
        for (var name in node) {
          var label = "";
          var qty = node[name];
          if(qty!==1){
            label += Math.round10(qty, -4) + " x ";
          }
          label += name;
          paths.push(label);
        }
      });
      text+=paths.join(' ➞ ');
      text+="<br/>";
    });
    
    //Calculate the dialogID
    showPaths.dialog = showPaths.dialog || 1;
    showPaths.dialog++;
    
    //Create the visualization tree
    text += "<br/><strong>Built from:</strong><br/>"
    //text += '<button id="collapse-all' + showPaths.dialog + '">Collapse All</button> <button id="expand-all' + showPaths.dialog + '">Expand All</button>';
    text += '<div id="design-tree' + showPaths.dialog + '"></div>';
    
    //Create the dialog
    var diagID = "dialogpath" + showPaths.dialog
    $("#plugin-footer").append('<div id="' + diagID + '" title="&nbsp;' + sku + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + '"></div>');
    $("#"+diagID).html(text);
    $("#"+diagID).dialog({
     height:'auto',
     width:'auto',
     minHeight: 'auto',
     resizable: false,
     beforeClose: function(){
       $( "#"+diagID ).parent().hide(); //prevents whole page from scrolling when dialog is closed
       return false;
     },
     dialogClass: "custom-dialog-1",
     open: function(){
      // render the tree
      setTimeout(function(){
        treeNodes('#design-tree' + showPaths.dialog, sku, 1, data, analysis, {"noToolTip": false, "height": 200, "width": 400});
      },0);
     }
    });
    $("#"+diagID).css("maxHeight", 700);
    dialogsToClose.push("#"+diagID);
    
    //Fix a weird JqueryUI bug (maybe a CSS conflict somewhere with SOS)
    var label = $("div.ui-dialog-titlebar.ui-corner-all.ui-widget-header.ui-helper-clearfix.ui-draggable-handle > button").html("");
    
   
    /*$('#collapse-all' + showPaths.dialog).click(function(){
        tree.collapseAll();
        event.preventDefault();
    });
    $('#expand-all' + showPaths.dialog).click(function(){
        tree.expandAll();
        event.preventDefault();
    });*/
  };

  $("#tabulator-inventory").tabulator({
      //height:650, // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
      layout:"fitColumns", //fit columns to width of table (optional)
      rowFormatter:function(row){
          if(row.getData().inventory<0){
              row.getElement().css({"background-color":"#FF4F4F"});
          }
      },
      data: tabledata,
      columns:[ //Define Table Columns
          {formatter:treeIcon, width:36, align:"center", headerSort:false, cellClick: showPaths},
          {title:"SKU", field:"sku", width:75, headerFilter:true},
          {title:"Supplier", field:"supplier", width:150, tooltip:true, headerFilter:true},
          {title:"Raw BOM Requirements", field:"bom", width:150},
          {title:"On Hand", field:"inventory", width:70, formatter:whereIcon, align:"right", cellClick: showLocationsWrapper},
          {title:"On PO", field:"onPO", width:70},
          {title:"Extra Needed", field:"neededPO", width:100},
          {title:"Can Build Now", field:"canBuildNow", width:100},
          {title:"Can Build Soon", field:"canBuildSoon", width:100},
          {title:"Build Status", field:"buildStatus", width:100, formatter: formatBuildStatus, headerFilter:true},
          {title:"Description", field:"description", tooltip:true, headerFilter:true},
          {title:"Notes", field:"notes", tooltip:true, headerFilter:true},
      ],
      /*rowClick:function(e, row){ //trigger an alert message when the row is clicked
          console.log("Row " + row.getData().id + " Clicked!!!!");
      },*/
  });
  
  // Prevent the mouse scroll from moving the whole page while scrolling in the table, and when limits reached.
  /*$( "#tabulator-inventory > div.tabulator-tableHolder" ).on('mousewheel DOMMouseScroll', function(e) {
    var scrollTo = null;

    if(e.type === 'mousewheel') {
       scrollTo = (e.originalEvent.wheelDelta * -1);
    }
    else if(e.type === 'DOMMouseScroll') {
       scrollTo = 40 * e.originalEvent.detail;
    }

    if(scrollTo) {
       e.preventDefault();
       $(this).scrollTop(scrollTo + $(this).scrollTop());
    }
  });*/
}

render.footer = function(sosParse, analysis){
  $("#plugin-footer").append("<strong>On Hand:</strong> Please make a physical count and ensure the inventory numbers are correct before starting a production batch.");
  $("#plugin-footer").append("<br/><br/><strong>Raw BOM Requirements:</strong> how many of this is required to build the complete design. It does not take into account on hand or on PO quantities.");
  $("#plugin-footer").append("<br/><br/><strong>Extra Needed:</strong> how many of this is missing to complete the build. Will need to be built or purchased in order to complete the build. Note that the calculation assumes PO's are received. You should also understand that if you purchase parent assembly items, the needed quantity will reduce for it's child items.");
  $("#plugin-footer").append("<br/><br/><strong>Can Build Now:</strong> how many of these items could be directly assembled with what is on hand. These numbers do not necessarily mean you can simultaneously build all of these, since some assemblies might share common parts. Also, be careful if you plan to keep spare parts by only building what is needed.");
  $("#plugin-footer").append('<br/><br/><strong>Can Build Soon:</strong> how many of these items will be possible to assemble after receiving PO' + "'" + 's or after building sub-assemblies first. WARNING: this is assuming you build following the "Extra Needed" quantities, and not the "Can Build Now" quantities. Building all the "Can build now" quantities could lower the result if it consumes items needed by other builds in the assembly.');
  $("#plugin-footer").append("<br/><br/><strong>Build Status:</strong> helps you know quickly which items are ready to be assembled, and which ones need additional purchases:");
  $("#plugin-footer").append('<br/>&nbsp;&nbsp;&nbsp;&nbsp;<span class="dot-white"></span>&nbsp;&nbsp;No more needed, enough on hand<br/>&nbsp;&nbsp;&nbsp;&nbsp;<span class="dot-green"></span>&nbsp;&nbsp;Ready to build all needed<br/>&nbsp;&nbsp;&nbsp;&nbsp;<span class="dot-yellow"></span>&nbsp;&nbsp;Soon receiving or soon able to build<br/>&nbsp;&nbsp;&nbsp;&nbsp;<span class="dot-red"></span>&nbsp;&nbsp;Items missing to build, need to purchase more<br/>&nbsp;&nbsp;&nbsp;&nbsp;<span class="dot-grey"></span>&nbsp;&nbsp;NON-INVENTORY item *');
  $("#plugin-footer").append('<br/><br/>*: manually check quantities prior to production. The description of the item must contain NON&#8209;INVENTORY as text, because the plugin is not able to know an item is NON&#8209;INVENTORY from the table given by SOS Inventory.');
  
  // Self test
  if(analysis.testsPassed){
    $("#plugin-footer").append('<br/><br/><span style="color:green">All internal tests passed.</span>')
  }else{
    $("#plugin-footer").append('<br/><br/><span class="warningText">TESTS FAILED</span>')
  }
  
  $("#plugin-footer").append("<br/>The content above was partly generated by the Expanded BOM plugin, written by Dominic Robillard. It is not endorsed or supported by SOS Inventory.<br/>dominic.robillard@rcbenchmark.com");
}