Helps you plan an assembly batch if using the SOS inventory software. It gives additional details in the expanded BOM report such as a shopping list. Note that this software is a third-party plugin provided as is, and is not supported/developped by SOS Inventory.
Installation: use the published chrome web extension here:

https://chrome.google.com/webstore/detail/gaagnhhhedijgpocmjncgomnoheibalo