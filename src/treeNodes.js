/*no-unused-vars
    treeNodes
*/

/*global
    treeNodes
*/

// *************** GENERATES THE INTERACTIVE VISUALIZATION TREE *********************

var treeNodes = function(canvas, root, buildQty, data, analysis, options){
    // canvas is a selector to a div that will hold the drawing
    // options struct: width, height, noToolTip
    
    //Adapted from: http://bl.ocks.org/robschmuecker/7880033
    /*Copyright (c) 2013-2016, Rob Schmuecker
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

    * The name Rob Schmuecker may not be used to endorse or promote products
      derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL MICHAEL BOSTOCK BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/
    
    // converts the given data to a format this tree graph will accept.
    var treeData = {
        "name": root,
        "size": buildQty
    };
    function buildChildren(treeData){        
        // Recusrsive function that populates the children nodes
        $.each(data.design[treeData.name], function( name, size ) {
            var childTree = {
                "name": name,
                "size": size
            }
            buildChildren(childTree);
            if(!treeData.hasOwnProperty("children")){
                treeData.children = [];
            }
            treeData.children.push(childTree);
        });
    }
    buildChildren(treeData);
        
    // Calculate total nodes, max label length
    var totalNodes = 0;
    var maxLabelLength = 0;
    // variables for drag/drop
    var selectedNode = null;
    var draggingNode = null;
    // panning variables
    var panSpeed = 200;
    var panBoundary = 20; // Within 20px from edges will pan when dragging.
    // Misc. variables
    var i = 0;
    var duration = 750;
    var root;
  
    // setup the notification about the stock status (user can do i on keyboard)
    var checkLocationForSku = null;
    $(document).keypress(function(event) {
      if(!checkLocationForSku)
          return;
      if(event.charCode === 105) // i
        showLocations(checkLocationForSku);
      checkLocationForSku = null;
    });

    // size of the diagram
    //console.log(options);
    var viewerWidth = options.width || 700;
    var viewerHeight = options.height || 700;

    var tree = d3.layout.tree()
        .size([viewerHeight, viewerWidth]);

    // define a d3 diagonal projection for use by the node paths later on.
    var diagonal = d3.svg.diagonal()
        .projection(function(d) {
            return [d.y, d.x];
        });

    // A recursive helper function for performing some setup by walking through all nodes

    function visit(parent, visitFn, childrenFn) {
        if (!parent) return;

        visitFn(parent);

        var children = childrenFn(parent);
        if (children) {
            var count = children.length;
            for (var i = 0; i < count; i++) {
                visit(children[i], visitFn, childrenFn);
            }
        }
    }

    // Call visit function to establish maxLabelLength
    visit(treeData, function(d) {
        totalNodes++;
        maxLabelLength = Math.max(getNodeText(d).length, maxLabelLength);

    }, function(d) {
        return d.children && d.children.length > 0 ? d.children : null;
    });
    
    // Returns what is printed besides the nodes.
    function getNodeText(d){
        if(d.size !== 1){
            return Math.round10(d.size, -4) + " x " + d.name;
        }
        return d.name;
    }


    // sort the tree according to the node names

    function sortTree() {
        tree.sort(function(a, b) {
            return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
        });
    }
    // Sort the tree initially incase the JSON isn't in a sorted order.
    sortTree();

    // TODO: Pan function, can be better implemented.

    function pan(domNode, direction) {
        var speed = panSpeed;
        if (panTimer) {
            clearTimeout(panTimer);
            translateCoords = d3.transform(svgGroup.attr("transform"));
            if (direction == 'left' || direction == 'right') {
                translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                translateY = translateCoords.translate[1];
            } else if (direction == 'up' || direction == 'down') {
                translateX = translateCoords.translate[0];
                translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
            }
            scaleX = translateCoords.scale[0];
            scaleY = translateCoords.scale[1];
            scale = zoomListener.scale();
            svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
            d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
            zoomListener.scale(zoomListener.scale());
            zoomListener.translate([translateX, translateY]);
            panTimer = setTimeout(function() {
                pan(domNode, speed, direction);
            }, 50);
        }
    }

    // Define the zoom function for the zoomable tree

    function zoom() {
        svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }


    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

    /*function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');

        svgGroup.selectAll("g.node").sort(function(a, b) { // select the parent and sort the path's
            if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
            else return -1; // a is the hovered element, bring "a" to the front
        });
        // if nodes has children, remove the links and nodes
        if (nodes.length > 1) {
            // remove link paths
            links = tree.links(nodes);
            nodePaths = svgGroup.selectAll("path.link")
                .data(links, function(d) {
                    return d.target.id;
                }).remove();
            // remove child nodes
            nodesExit = svgGroup.selectAll("g.node")
                .data(nodes, function(d) {
                    return d.id;
                }).filter(function(d, i) {
                    if (d.id == draggingNode.id) {
                        return false;
                    }
                    return true;
                }).remove();
        }

        // remove parent link
        parentLink = tree.links(tree.nodes(draggingNode.parent));
        svgGroup.selectAll('path.link').filter(function(d, i) {
            if (d.target.id == draggingNode.id) {
                return true;
            }
            return false;
        }).remove();

        dragStarted = null;
    }*/

    // define the baseSvg, attaching a class for styling and the zoomListener
    d3.select(canvas)
        .attr("class", "tree-nodes")	
    var baseSvg = d3.select(canvas).append("svg")
        .attr("width", viewerWidth)
        .attr("height", viewerHeight)
        .attr("class", "overlay")
        .call(zoomListener);


    // Define the drag listeners for drag/drop behaviour of nodes.
    /*dragListener = d3.behavior.drag()
        .on("dragstart", function(d) {
            if (d == root) {
                return;
            }
            dragStarted = true;
            nodes = tree.nodes(d);
            d3.event.sourceEvent.stopPropagation();
            // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on("drag", function(d) {
            if (d == root) {
                return;
            }
            if (dragStarted) {
                domNode = this;
                initiateDrag(d, domNode);
            }

            // get coords of mouseEvent relative to svg container to allow for panning
            relCoords = d3.mouse($('svg').get(0));
            if (relCoords[0] < panBoundary) {
                panTimer = true;
                pan(this, 'left');
            } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                panTimer = true;
                pan(this, 'right');
            } else if (relCoords[1] < panBoundary) {
                panTimer = true;
                pan(this, 'up');
            } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                panTimer = true;
                pan(this, 'down');
            } else {
                try {
                    clearTimeout(panTimer);
                } catch (e) {

                }
            }

            d.x0 += d3.event.dy;
            d.y0 += d3.event.dx;
            var node = d3.select(this);
            node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");
            updateTempConnector();
        }).on("dragend", function(d) {
            if (d == root) {
                return;
            }
            domNode = this;
            if (selectedNode) {
                // now remove the element from the parent, and insert it into the new elements children
                var index = draggingNode.parent.children.indexOf(draggingNode);
                if (index > -1) {
                    draggingNode.parent.children.splice(index, 1);
                }
                if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
                    if (typeof selectedNode.children !== 'undefined') {
                        selectedNode.children.push(draggingNode);
                    } else {
                        selectedNode._children.push(draggingNode);
                    }
                } else {
                    selectedNode.children = [];
                    selectedNode.children.push(draggingNode);
                }
                // Make sure that the node being added to is expanded so user can see added node is correctly moved
                expand(selectedNode);
                sortTree();
                endDrag();
            } else {
                endDrag();
            }
        });*/

    /*function endDrag() {
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
        updateTempConnector();
        if (draggingNode !== null) {
            update(root);
            centerNode(draggingNode);
            draggingNode = null;
        }
    }*/

    // Helper functions for collapsing and expanding nodes.

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    function expand(d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }

    var overCircle = function(d) {
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };

    // Function to update the temporary connector indicating dragging affiliation
    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
            // have to flip the source coordinates since we did this for the existing connectors on the original tree
            data = [{
                source: {
                    x: selectedNode.y0,
                    y: selectedNode.x0
                },
                target: {
                    x: draggingNode.y0,
                    y: draggingNode.x0
                }
            }];
        }
        var link = svgGroup.selectAll(".templink").data(data);

        link.enter().append("path")
            .attr("class", "templink")
            .attr("d", d3.svg.diagonal())
            .attr('pointer-events', 'none');

        link.attr("d", d3.svg.diagonal());

        link.exit().remove();
    };

    // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

    function centerNode(source, root) {
        //console.log('Centering node: ' + source.name);
        scale = zoomListener.scale();
        x = -source.y0;
        y = -source.x0;
        if(root){
          x = x * scale + viewerWidth / 6;
        }else{
          x = x * scale + viewerWidth / 2;
        }
        y = y * scale + viewerHeight / 2;
        baseSvg.select('g').transition()
            .duration(duration)
            .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
        zoomListener.scale(scale);
        zoomListener.translate([x, y]);
    }

    // Toggle children function
    function toggleChildren(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else if (d._children) {
            d.children = d._children;
            d._children = null;
        }
        return d;
    }

    // Toggle children on click
    function click(d) {
        //console.log("clicked " + d.name);
        //if (d3.event.defaultPrevented) return; // click suppressed
        d = toggleChildren(d);
        update(d);
        centerNode(d);
    }
    
    // Show/hide tooltip on hover
    // Define the div for the tooltip
    var div = d3.select("body").append("div")	
        .attr("class", "tree-nodes tooltip")		
        .style("opacity", 0)
        .style("z-index", 1500); // more than the popup window that contains the graph.
    
    function showTooltip(d){
        if(options.noToolTip === true) return;
      
        //Get the mouse coordinates
        var y = (d3.event.pageY + 16);
        var x = (d3.event.pageX + 16);
        
        //Prepare the tooltip content
        var html;
        var isInfinity = !isFinite(data.inventory[d.name]);
        var qty = Math.round10(data.inventory[d.name], -4);
        var onPO = data.onPO[d.name];
        var supplier = data.supplier[d.name];
        var neededPO = analysis.neededPO[d.name];
        var canBuildNow = analysis.canBuildNow[d.name];
        var canBuildNowPO = analysis.canBuildNowPO[d.name];
        var canBuildSoon = analysis.buildResultsPO[d.name].buildQty
        var description = data.description[d.name];
        var bomQty = analysis.buildList[d.name];
        var parents = data.parents[d.name];
        
        if(parents === undefined){
          parents = [];
        }
      
        if(description.length>300){
            description = description.substring(0,297) + "...";
        }
      
        // Detects if drawing the whole tree or a "subtree".
        var isSubTreeRoot = d.parent === undefined;
      
        //Prepare the notes
        var notes = "";
        $.each(data.notes[d.name], function(parent,note){
          if(note!=="" && !isSubTreeRoot && parent === d.parent.name){
            if(note.length>100){
                note = note.substring(0,97) + "...";
            }
            notes += "For " + parent + ": " + note + "<br/>";
          }
        });
      
        var html = "";
        function writeBuild(){
          html += "<br/>";
          var list = [];
          
          $.each(parents, function(parent, qty){
            if(!isSubTreeRoot && parent!==d.parent.name){
              if(qty!=1){
                list.push(parent + " (x" + qty + ")");
              }else{
                list.push(parent);
              }
            }
          });
          if(list.length>0){
            html += "<strong>Also used in:</strong> ";
            html += list.join(", ") + "<br/><br/>";
          }
          html += "Raw BOM Requirements: " + bomQty + "<br/>";
          if(neededPO > 0){
            html += "Extra Needed: " + neededPO + "<br/>";
          }
          if(d.children || d._children){
            html += "Can build now: " + canBuildNow + "<br/>";
            html += "Can build soon: " + canBuildSoon + "<br/>";
          }
          //html += "<br/>";
        }
      
        if(d.name !== data.rootName || isSubTreeRoot){
            if(supplier !== ""){
                html += supplier + "<br/>";
            }
            html += "<strong>" + description + '</strong><br/><br/>';
          
            if(isInfinity){
              html += "Non-inventory<br/>";
            }else{
              if(qty<0){
                html += '<strong><span style="color: red">On hand: ' + qty + '</span></strong><br/>';
              }else{
                  html += "On hand: " + qty + " (press <strong>i</strong> to get location info)<br/>";
              }
              html += "On PO: " + onPO + "<br/>";
            } 
            
          
            writeBuild();
            
            if(notes!==""){
                html +="<br/><strong>Notes:</strong><br/>" + notes;
            }
        }else{
            html = "<strong>Top level assembly: " + d.name + '</strong><br/>';
            writeBuild();
        }
      
        //set the callback i value
        checkLocationForSku = d.name;
        
        //Generate the tooltip
        div.transition()		
           .duration(500)		
           .style("opacity", .9);		
        div.html(html)	
           .style("left", x + "px")		
           .style("top", y + "px");	
    }
    function hideTooltip(d){
        if(options.noToolTip === true) return;
      
        div.transition()		
           .duration(500)		
           .style("opacity", 0);	
      
        checkLocationForSku = null;
    }

    function update(source) {
        // Compute the new height, function counts total children of root node and sets tree height accordingly.
        // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
        // This makes the layout more consistent.
        var levelWidth = [1];
        var childCount = function(level, n) {

            if (n.children && n.children.length > 0) {
                if (levelWidth.length <= level + 1) levelWidth.push(0);

                levelWidth[level + 1] += n.children.length;
                n.children.forEach(function(d) {
                    childCount(level + 1, d);
                });
            }
        };
        childCount(0, root);
        var newHeight = d3.max(levelWidth) * 35; // 25 pixels per line  
        tree = tree.size([newHeight, viewerWidth]);
      
        // Returns the node fill color depending on analysis results.
        function getNodeFillColor(d) {
            var color = "#fff"; //white default

            // Circle color depending on build stat
            switch (analysis.buildStatus[d.name]) {
              case "Missing":
                color = "red";                    
                break;
              case "Ready to build":
                color = "green";
                break;
              case "Can build soon":
                color = "yellow";
                break;
              case "Non-inventory":
                color = "grey";
                break;
            }
            return color;
        }
      
        // Used to set nodes in RED when quantities are negative
        function getNodeStrokeColor(d){
          var color = "#202020";
          if(data.inventory[d.name]<0){
            color = "#FF4F4F";
          }
          return color;
        }
      
        // Returns the nodes expand label
        function getNodeExpandLabel(d){
            if(d._children){
                return "+";
            }
            return "";
        }

        // Gets the node qty as string
        // Don't display "1"
        function getNodeQty(d){
            return d.size;
        }

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);
      
        var dx = 14; //text distance from circle center

        // Set widths between levels based on maxLabelLength.
        nodes.forEach(function(d) {
            //d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
            // alternatively to keep a fixed scale one can set a fixed depth per level
            // Normalize for fixed-depth by commenting out below line
            d.y = (d.depth * 110); //px per level.
        });

        // Update the nodes…
        node = svgGroup.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id || (d.id = ++i);
            });

        // Enter any new nodes at the parent's previous position.
        // Makes nodes appear as poping from their parents in the animation
        var nodeEnter = node.enter().append("g")
            //.call(dragListener)
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on('click', click)
            .on('mouseover', showTooltip)
            .on('mouseout', hideTooltip);

        // New nodes start at 0 radius, and at correct color
        nodeEnter.append("circle")
            .attr('class', 'nodeCircle')
            .attr("r", 0)
            .style("fill", getNodeFillColor)
            .style("stroke", getNodeStrokeColor);

        // Text for new nodes starts fully transparent
        nodeEnter.append("text")
            .attr("x", function(d) {
                return d.children || d._children ? -dx : dx;
                //return 0;
            })
            .attr("dy", ".35em")
            .attr('class', 'nodeText')
            .attr("text-anchor", function(d) {
                return d.children || d._children ? "end" : "start";
                //return "middle";
            })
            .text(getNodeText)
            .style("fill-opacity", 0);
      
        // Node inside text
        nodeEnter.append("text")
            .attr("x", function(d) {
                //return d.children || d._children ? -dx : dx;
                return 0;
            })
            .attr("dy", ".35em")
            .attr('class', 'expandText')
            .attr("text-anchor", function(d) {
                //return d.children || d._children ? "end" : "start";
                return "middle";
            })
            .text(getNodeExpandLabel)
            .style("fill-opacity", 0);

        // Node left text (quantity used)
        /*nodeEnter.append("text")
            .attr("x", function(d) {
                //return d.children || d._children ? -dx : dx;
                return -14;
            })
            .attr("dy", ".35em")
            .attr('class', 'qtyText')
            .attr("text-anchor", function(d) {
                //return d.children || d._children ? "end" : "start";
                return "end";
            })
            .text(getNodeQty)
            .style("fill-opacity", 0);*/


        // phantom node to give us mouseover in a radius around it
        /*nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", 30)
            .attr("opacity", 0.2) // change this to zero to hide the target area
        .style("fill", "red")
            .attr('pointer-events', 'mouseover')
            .on("mouseover", function(node) {
                overCircle(node);
            })
            .on("mouseout", function(node) {
                outCircle(node);
            });*/

          node.select('text.nodeText')
              .attr("x", function(d) {
                  return d.children || d._children ? -dx : dx;
                  //return 0;
              })
              .attr("y", function(d) {
                  //return d.children || d._children ? -dx : dx;
                  return 0;
              })
              .attr("text-anchor", function(d) {
                  return d.children || d._children ? "end" : "start";
                  return "middle";
              })
              .text(getNodeText);

          node.select('text.expandText')
              .attr("x", function(d) {
                  //return d.children || d._children ? -dx : dx;
                  return 0;
              })
              .attr("y", function(d) {
                  //return d.children || d._children ? -dx : dx;
                  return 0;
              })
              .attr("text-anchor", function(d) {
                  //return d.children || d._children ? "end" : "start";
                  return "middle";
              })
              .text(getNodeExpandLabel);

          /*node.select('text.qtyText')
              .attr("x", function(d) {
                  //return d.children || d._children ? -dx : dx;
                  return -14;
              })
              .attr("y", function(d) {
                  //return d.children || d._children ? -dx : dx;
                  return 0;
              })
              .attr("text-anchor", function(d) {
                  //return d.children || d._children ? "end" : "start";
                  return "end";
              })
              .text(getNodeQty);*/


        // Set the circle fill
        node.select("circle.nodeCircle")
            .attr("r", 8)
            .style("fill", getNodeFillColor);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + d.y + "," + d.x + ")";
            });

        // Fade the text in
        nodeUpdate.select("text.expandText")
            .style("fill-opacity", 1);

        nodeUpdate.select("text.nodeText")
            .style("fill-opacity", 1);

        /*nodeUpdate.select("text.qtyText")
            .style("fill-opacity", 1);*/

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 0);

        nodeExit.select("text.expandText")
            .style("fill-opacity", 0);
        nodeExit.select("text.nodeText")
            .style("fill-opacity", 0);
        /*nodeExit.select("text.qtyText")
            .style("fill-opacity", 0);*/


        // Update the links…
        var link = svgGroup.selectAll("path.link")
            .data(links, function(d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                var o = {
                    x: source.x0,
                    y: source.y0
                };
                return diagonal({
                    source: o,
                    target: o
                });
            });

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
                var o = {
                    x: source.x,
                    y: source.y
                };
                return diagonal({
                    source: o,
                    target: o
                });
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }
  
    function collapseAll(){
      tree.nodes(root).reverse().forEach(function(d){
      //if(d.depth === 0)
          collapse(d);
      });
      update(root);
      centerNode(root);
    }
  
    function expandAll(){
      tree.nodes(root).reverse().forEach(function(d){
      //if(d.depth === 0)
          expand(d);
      });
      update(root);
      centerNode(root);
    }

    // Append a group which holds all nodes and which the zoom Listener can act upon.
    var svgGroup = baseSvg.append("g");

    // Define the root
    root = treeData;
    root.x0 = viewerHeight / 2;
    root.y0 = 0;
  
    // Initial state based on the total number of nodes (we don't want too many!)
    /*console.log(totalNodes);

    // Layout the tree initially and center on the root node.
    tree.nodes(root).reverse().forEach(function(d){
      if(d.depth === 0)
        collapse(d);
    });*/
  
    update(root);
    centerNode(root, true);
    
    // PUBLIC FUNCTIONS
    return {
        collapseAll: collapseAll,
        expandAll: expandAll
    }
};
