// *************** HANDLES DISPLAY OF TABS *********************

var tabs = {};

// Initializes the tab system
tabs.init = function init(htmlDiv){
  tabs.htmlDiv = htmlDiv;
  var html = [
    '<div class="wrapper">',
    '  <ul class="tabs clearfix" data-tabgroup="first-tab-group">',
    '  </ul>',
    '  <section id="first-tab-group" class="tabgroup">',
    '  </section>',
    '</div>'
  ].join('');
  $(htmlDiv).append(html);
}

// Creates a new tab, calls callback when tab is shown
tabs.add = function add(id, name, callback){
  tabs.callbacks = tabs.callbacks || {};
  tabs.callbacks[id] = callback;
  
  // Add the tab button
  $(tabs.htmlDiv + " > div > ul").append('<li><a href="#tab' + id + '" id="' + id + 'tabbutton">' + name + '</a></li>');
  
  // Add it's content div
  var html = [
    '<div class="tab-content" id="tab' + id + '">',
    '</div>'
  ].join('');
  $("#first-tab-group").append(html);
  
  // Hide all the sections except the first
  $('.tabgroup > div').hide();
  $('.tabgroup > div:first-of-type').show();
  
  $('#' + id + 'tabbutton').click(function(e){
    e.preventDefault();
    var $this = $(this),
        tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
        others = $this.closest('li').siblings().children('a'),
        target = $this.attr('href');
    others.removeClass('active');
    $this.addClass('active');
    $(tabgroup).children('div').hide();
    $(target).show();
    if(tabs.callbacks[id]) tabs.callbacks[id]();
  });
}

// Renames a tab (keeps the same ID)
tabs.rename = function rename(id, newName){
  console.error("Function not yet implemented");
}

// Removes a new tab
tabs.remove = function remove(id){
  console.error("Function not yet implemented");
}

// Sets a tab's content
tabs.content = function content(id, html){
  tabs.clear(id);
  tabs.append(id, html);
}

// Appends to a tab's content
tabs.append = function append(id, html){
  $('div#tab' + id).append(html);
}

// Clears a tab
tabs.clear = function clear(id){
  $('div#tab' + id).empty();
}
