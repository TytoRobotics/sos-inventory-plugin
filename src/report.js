// *************** TAKES CARE OF REINTERPRETING THE DATA TO GAIN VALUABLE INFO *********************

function report(data){
    var results = {};
    
    // Handle the special CURRENT_BATCHES case.
    var assumeZero = false;
    if(data.rootName === "CURRENT_BATCHES"){
      $("#plugin-header").append('<span class="warningText"> Special item "CURRENT_BATCHES" is used. Since this item is to help us track production batches, everything on the FIRST LEVEL of this special SKU is assumed to have zero stock in the reports. It is important you understand the reasons and consequences of this. See the RCbenchmark Wiki for details.</span><br/>');
      assumeZero = true;
      //alert("Because of performance reasons, CURRENT_BATCHES is not supported anymore.")
    }
  
    // ****************************** PRINT FUNCTIONS - TEMPORARY UNTIL GUI IS DONE ************************  
    function log(text) {
      tabs.append('report', text + "<br/>");
    }

    function logBold(text) {
      log('<strong>' + text + '</strong>');
    }

    // prints the content of an object. Can use the toString method on objects
    function printObj(obj, prefix){
       prefix = prefix || '&nbsp;&nbsp;&nbsp;&nbsp;';
       var result = '';
       var rootName = data.rootName;
       var supplier = data.supplier;
       var description = data.description;
       forEach(obj,function(key){
         result += prefix + key;
         var additionnalText = "&nbsp;&nbsp;&nbsp;&nbsp;- ";
         if(key!==rootName && supplier[key]!==""){
           additionnalText += supplier[key] + ' - ';
         }
         var trimmedDescription;
         if(key === rootName){
           trimmedDescription = "The report is for this item";
         }else{
           if(description[key]===undefined){
             trimmedDescription = '';
           }else{
             trimmedDescription = description[key].substring(0, 100); // limit description length.
             if(trimmedDescription !== description[key]){
               trimmedDescription += " ...";
             }
           }
         }
         additionnalText += trimmedDescription;
         if (typeof obj[key] == 'object'){
           result += additionnalText + '<br/>' + printObj(obj[key], prefix + prefix);
         } else {
           result += ' x ' + obj[key] + additionnalText + '<br/>';
         }
       })
       log(result);
    }

    function printReports(reports){
      var inventory = reports.inventory;
      var inventoryPO = reports.inventoryPO;
      var po = reports.po;
      var name = reports.name;
      var items = reports.items;
      var buildQty = reports.buildQty;
      var buildResults = reports.buildResults;
      var buildResultsPO = reports.buildResultsPO;
      var buildList = reports.buildList;
      var needed = reports.needed;
      var neededPO = reports.neededPO;
      var exe = reports.exe;
      var exePO = reports.exePO;
      var buildRemainder = reports.buildRemainder;
      var buildRemainderPO = reports.buildRemainderPO;

      $("#reportContainer").empty();

      // Give visual feedback that the report is regenerating after pressing analyze again
      setTimeout(function (){
        //logBold("--- REPORT FOR " + buildQty + " x " + name + " ---");
        logBold("Design structure (make sure this is correct):");
        printObj(items);

        logBold("Inventory:");
        log("Please make a physical count and ensure the inventory numbers are correct before starting a production batch.");
        printObj(inventory);

        // only print non-zero ones
        logBold("PO list:");
        var polist = {};
        forEach(po, function(sku){
          if(po[sku] != 0){
            //console.log(po[sku]);
            polist[sku] = po[sku];
          }
        });
        if(isEmpty(polist)){
          log("No items on PO for this build.<br/>");
        }else{
          printObj(polist);
          /*logBold("Inventory assuming PO's received:");
          inventoryPO.printStruct();*/
        }

        logBold("How many can be assembled right now (PO excluded)?");
        log("&nbsp;&nbsp;&nbsp;&nbsp;" + buildResults.buildQty + " x");
        log("&nbsp;&nbsp;&nbsp;&nbsp;For more copies add: " + buildResults.missingItem + "<br/>");

        logBold("How many can be assembled after PO's received?");
        log("&nbsp;&nbsp;&nbsp;&nbsp;" + buildResultsPO.buildQty + " x");
        log("&nbsp;&nbsp;&nbsp;&nbsp;For more copies add: " + buildResultsPO.missingItem + "<br/>");

        logBold("Items needed for " + buildQty + " full build");
        log("This is the complete BOM for the selected item");
        printObj(buildList);

        /*logBold("What is missing based on inventory to make " + buildQty + "?");
        needed.printStruct();*/

        /*logBold("What is missing in stock to make " + buildQty + "?");
        log("Positive means we need those otherwise assembly cannot complete. Does not account for PO's since they are not in stock yet.");
        exe.buy.printStruct();*/

        /*logBold("What needs to be assembled (ignores PO) to make " + buildQty + "?");
        exe.assemble.printStruct();*/

        logBold("What still needs to be purchased to make " + buildQty + "?");
        log("Positive means we need to order more, considering existing PO's.");
        printObj(exePO.buy);

        logBold("What needs to be assembled to make " + buildQty + "?");
        log("Assemble end of list first, assumes POs have been received. Use this list during production to limit how many of each assembly step to make, which will ensure there is no step consuming all the parts if the intention is to keep spare parts in stock.");
        printObj(exePO.assemble);

        // If a build can be completed at requested qty, then show what will be left in the inventory
        /*if(buildResults.buildQty >= buildQty){
          logBold("What is left in stock after assembly of " + buildQty + "?");
          log("Assumes you do the assembly immediatly (ignore PO's). Use this to see how many spare parts you will still have.");
          printObj(buildRemainder.inventory);
        }*/

        // If a build can be completed at requested qty, then show what will be left in the inventory
        /*if(buildResultsPO.buildQty >= buildQty){
          logBold("What is left in stock after assembly of " + buildQty + "?");
          log("Assumes you do the assembly after all PO's received. Use this to see how many spare parts you will still have.");
          printObj(buildRemainder.inventory);
        }*/

        log("Need more? Talk to Dominic about your ideas.");
        logBold("**** End of report ****");
      }, 200);
    };

    // ******************* ANALYZE THE DATA *****************************
    // checks if object is empty
    function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }

    // iterates an object properties
    function forEach(obj,callback) {
      for(var index in obj) { 
         if (obj.hasOwnProperty(index)) {
             if(callback) callback(index);
         }
      }
    }

    // checks if two objects are equal
    function equalObjs(a,b){
      var aStr = JSON.stringify(a);
      var bStr = JSON.stringify(b);
      if(aStr === bStr){
        return true;
      }else{
        console.log("Different strings:");
        console.log(aStr);
        console.log(bStr);
        return false;
      }
      return aStr === bStr;
    }

    //Get flat dependencies count, iteratively
    function getFlatDeps(items, key, qty, result){
      if (result === undefined){
        result = {};
      }
      var item = items[key];

      //Add item to list
      if(result[key] === undefined){
        result[key] = qty;
      }else{
        result[key] += qty;
      }
      if(!isEmpty(item)) {
        forEach(item, function (subkey){
          var qtyNeeded = qty * item[subkey];
          getFlatDeps(items, subkey, qtyNeeded, result);
        });
      }
      return result;
    }
  
    //Recursive function that returns a structure containing all the paths of the design
    function getPaths(items, root, qty){
      var paths = [];

      var rootObj = {};
      rootObj[root] = qty || 1;
      
      forEach(items[root], function (subkey){
        var subQty = items[root][subkey];
        if(isAtomic(items,subkey, true)){
          var obj = {};
          obj[subkey] = subQty;
          paths.push([rootObj,obj]);
        }else{
          var subPaths = getPaths(items, subkey, subQty);
          subPaths.forEach(function(subPath){
            paths.push([rootObj].concat(subPath));
          });
        }
      });

      return paths;
    }
  
    //Determines the paths of where an item can be found
    function getUsedBy(paths, sku){
      var usedBy = [];
      paths.forEach(function(path){
        var resultArray = [];
        var found = false;
        path.forEach(function(node){
          if(!found){
            resultArray.push(JSON.parse(JSON.stringify(node)));
            if(node[sku]!==undefined){
              found = true;
            }
          }
        });
        if(found){
          usedBy.push(resultArray);
        }
      });
      
      // Remove duplicate lines
      var usedByNoDup = [];
      usedBy.forEach(function(line){
        var dup = false;
        var lineStr = JSON.stringify(line);
        usedByNoDup.forEach(function(line2){
          var line2Str = JSON.stringify(line2);
          if(lineStr === line2Str){
            dup = true;
          }
        });
        if(!dup){
          usedByNoDup.push(line);
        }
      });
      return usedByNoDup;
    }

    //Check if item is atomic component (cannot be divided in modules).
    //We consider atomic also if all child nodes have zero quantities.
    function isAtomic(items, key, acceptZero){
      var childs = 0;
      forEach(items[key], function(subKey){
        if(acceptZero){
          childs += 1; //include children that have zero multiplier
        }else{
          childs += items[key][subKey];
        }
      });
      return childs===0;
    }

    //Return what is needed after removing inventory list
    //Assumes we want list for ADDITIONAL copies
    //Also assumes we cannot "deconstruct" items (see test#11)
    function substractInventory(design, requirements, inventory, mainKey){
      // first make a copy so it can be modified
      var result = JSON.parse(JSON.stringify(requirements));

      // take what we have and substract from requirements
      forEach(requirements, function(key){
        var qtyInStock = inventory[key] || 0;
        if(mainKey === key){
          qtyInStock = 0; //Assume additional items 
        }else{
          if(qtyInStock>=result[key]){
            qtyInStock = result[key]; // Do not "deconstruct" items.
          }
          var flattenedInv = getFlatDeps(design, key, qtyInStock);

          // substract flattened inventory to flattened requirements
          forEach(flattenedInv, function(inventoryItem){
            if (result.hasOwnProperty(inventoryItem)) {
               result[inventoryItem] -= flattenedInv[inventoryItem];
               if(result[inventoryItem]<0){
                 result[inventoryItem] = 0; // Do not let go to negative.
               }
            }
          });
        }
      });
      return result;
    };

    // Reports what needs to be done
    function execution(items, needed){
      assemble = {};
      buy = {};
      forEach(needed, function(key){
        if(isAtomic(items, key)){
          buy[key] = needed[key];
        }else{
          assemble[key] = needed[key];
        }
      });
      return {assemble: assemble, buy: buy};
    }

    // Returns how many can be built
    function getBuildCapability(items, key, Originalinventory){
      if(isAtomic(items, key)){
          return {buildQty: 0, missingItem: key};
      }
      
      // Uses a binary search tree algorithm for finding the build quantity value.
      
      // Assume a build quantity
      var buildQty = 512;
      var buildFailed = NaN;
      var buildPass = NaN;
      var tolerance = 4; //significant digits
      var done = false;
      var missingItem;
      var iterations = 0;
      var maxIterations = 100;
      
      // Loop until result have enough accuracy (allows hanndling noninteger numbers)
      while(!done && ++iterations<maxIterations){
        var inventory = JSON.parse(JSON.stringify(Originalinventory));
        var buildList = getFlatDeps(items, key, buildQty);
        var needed = substractInventory(items, buildList, inventory, key);
        
        // Check if pass/fail. Basically, a build is possible if all root nodes have negative or zero needed quantities.
        var pass = true;
        forEach(needed,function(subKey){
          if(isAtomic(items,subKey)){
            if(needed[subKey]>0){
              pass = false;
              missingItem = subKey;
            }
          }
        });
        
        var maxError = 1/Math.pow(10,tolerance+1);
                
        // Determine what to try in the next iteration
        if(pass){
          buildPass = buildQty;
        }else{
          buildFailed = buildQty;
        }
        if(isNaN(buildFailed)){
          buildQty = buildQty*2;
        }else if(isNaN(buildPass)){
          buildQty = buildQty/2;
          // Exit condition
          if(buildQty<=maxError){
            done = true;
          }
        }else{
          buildQty = (buildPass+buildFailed)/2;
          // Exit condition
          if(Math.abs(buildFailed-buildPass)<=maxError){
            done = true;
          }
        }
      }
      
      if(iterations >= maxIterations){
        console.error("Report did too many iterations");
      }
      
      // Round to remove float errors (ie 4.000000000000114 should be 4)
      buildQty = Math.round10(buildQty, -tolerance);
      
      return {buildQty: buildQty, missingItem: missingItem};
    }
  
    // Returns the quantity that can be directly built (no expanded sub assemblies)
    function getDirectBuildCapability(items, name, inventory){
      //return 0 for atomic nodes (cannot be built)
      if(isAtomic(items,name)){
        return 0;
      }
      
      var qty = NaN; // assume infinity until proven otherwise
      var deps = items[name];
      // Calculate the maximum qty that can be consumed of each subitem and the minimum of those will be the answer.
      forEach(deps,function(subKey){
        var multiplier = deps[subKey];
        if(multiplier!==0){
          var canBuild = inventory[subKey]/multiplier;
          if(isNaN(qty)){
            qty = canBuild;
          }else{
            qty = Math.min(qty, canBuild);
          }
        }
      });
      if(isNaN(qty)){
        qty = 0;
      }
      return Math.round10(qty, -4);        
    }

    // nonInventory is an array of item names that are NON-INVENTORY
    // this means qty is zero, but also we assume we need zero in all quantities. The BOM can still have non-zero
    function generateReports(name, items, Originalinventory, po, buildQty, assumeFirstLevelZero, nonInventory){
      // DEFINITIONS https://docs.google.com/document/d/1Pgm9AvDJV7hH6nQ7-UZgnfJzraepudyoJn5F-7EAE5Y/edit?usp=sharing
      
      if(!nonInventory){
        nonInventory = [];
      }
      
      var report = {};
      report.print = function(){};
      if(isEmpty(items)){
        return report;
      }

      //Make a copy of the inventory
      var inventory = JSON.parse(JSON.stringify(Originalinventory));
      if(inventory[name] === undefined){
        inventory[name] = 0;
      }
      
      //If assume first level zero, them modify the inventory to reflect that
      if(assumeFirstLevelZero){
        var itemsInRoot = items[name];
        forEach(itemsInRoot, function(sku){
          inventory[sku] = 0;
        });
      }
      
      //Support infinity quantity for inventory items
      forEach(inventory, function (sku){
          if(!isFinite(Originalinventory[sku])){
              inventory[sku] = Infinity;
          }
      });

      report.inventory = inventory;
      report.po = po;
      report.name = name;
      report.items = items;
      report.buildQty = buildQty;

      //calculate the quantity with PO
      var inventoryPO = {};
      forEach(inventory, function (sku){
          inventoryPO[sku] = inventory[sku] + po[sku];
      });
      report.inventoryPO = inventoryPO;

      //calculate how many we can build
      var buildResults = {};//getBuildCapability(items, name, inventory);
      var buildResultsPO = {};//getBuildCapability(items, name, inventoryPO);
      forEach(inventory, function (sku){
          buildResults[sku] = getBuildCapability(items, sku, inventory);
          buildResultsPO[sku] = getBuildCapability(items, sku, inventoryPO);
      });
      report.buildResults = buildResults;
      report.buildResultsPO = buildResultsPO;

      // do a build of the exact quantity required to see what inventory will be left after.
      /*var buildRemainder = build(items, name, buildQty, inventory);
      delete buildRemainder.inventory[name];
      report.buildRemainder = buildRemainder;
      var buildRemainderPO = build(items, name, buildQty, inventoryPO);
      delete buildRemainderPO.inventory[name];
      report.buildRemainderPO = buildRemainderPO;*/

      var buildList = getFlatDeps(items, name, buildQty);
      report.buildList = buildList;

      var needed = substractInventory(items, buildList, inventory, name);
      report.needed = needed;

      var neededPO = substractInventory(items, buildList, inventoryPO, name);
      report.neededPO = neededPO;

      var exe = execution(items, needed);
      report.exe = exe;

      var exePO = execution(items, neededPO);
      report.exePO = exePO;
      
      // calculate the immediate build capability of each node
      var canBuildNow = {};
      var canBuildNowPO = {};
      forEach(inventory, function (sku){
          canBuildNow[sku] = getDirectBuildCapability(items, sku, inventory);
      });
      forEach(inventory, function (sku){
          canBuildNowPO[sku] = getDirectBuildCapability(items, sku, inventoryPO);
      });
      report.canBuildNow = canBuildNow;
      report.canBuildNowPO = canBuildNowPO;
      
      // determine the build status of each node. See Google doc at the top of this function for details.
      var buildStatus = {};
      forEach(inventory, function (sku){
        buildStatus[sku] = "Missing";
        if(!isFinite(inventory[sku])){
          buildStatus[sku] = "Non-inventory";
        }else if( needed[sku] <= 0){
          buildStatus[sku] = "Have enough";
        }else if(canBuildNow[sku] >= needed[sku]){
          buildStatus[sku] = "Ready to build";  
        }else if(buildResultsPO[sku].buildQty >= neededPO[sku]){
          buildStatus[sku] = "Can build soon";     
        } 
      });
      report.buildStatus = buildStatus;
      
      // Get the paths of all possible tree branches
      var paths = getPaths(items, name, buildQty);
      report.paths = paths;
      
      // For each nodes, get the paths where they appear (from root to item)
      var usedBy = {};
      forEach(inventory, function (sku){
        usedBy[sku] = getUsedBy(paths, sku);
      });
      report.usedBy = usedBy;

      report.print = function(){
        printReports(report);
      }
      return report;
    };

    //********************** TESTING ************************
    function testEngine(){
      var passed = true;

      // Data for tests 1 to 3
      var simpleDevice = {
        "device": {
          "box": 1,
          "tape_cm": 10,
          "wrapped_pcb": 1,
          "wrapped_hardware": 1
        },
        "wrapped_pcb": {
          "pcb": 1,
          "ziploc": 1
        },
        "wrapped_hardware": {
          "ziploc": 3,
          "screws": 2
        }
      };

      var simpleInventory = {
        "device": 4,
        "box": 6,
        "tape_cm": 50,
        "wrapped_hardware": 3,
        "wrapped_pcb": 2,
        "pcb": 8,
        "ziploc": 20
      };

      var simplePO = {
        "device": 0,
        "box": 0,
        "tape_cm": 0,
        "wrapped_hardware": 0,
        "wrapped_pcb": 0,
        "pcb": 0,
        "ziploc": 0
      };

      simpleReport = generateReports("device", simpleDevice, simpleInventory, simplePO, 5);

      // TEST 1 - REQUIREMENTS ARE CALCULATED PROPERLY
      // Also tests if the last parameter of generateReports is undefined
      expected = {
        "device": 5,
        "box": 5,
        "tape_cm": 50,
        "wrapped_pcb": 5,
        "pcb": 5,
        "ziploc": 20,
        "wrapped_hardware": 5,
        "screws": 10
      };
      if(equalObjs(expected, simpleReport.buildList)){
        //log("TEST 1 PASSED")
      }	else {
        console.log("TEST 1 FAILED");
        passed = false;
      }

      // TEST 2 - BUILD QTY FROM INVENTORY IS CORRECT
      var canBuild = simpleReport.buildResults["device"].buildQty - 3;
      var missing = simpleReport.buildResults["device"].missingItem;
      if(canBuild !== 0 || missing !== "screws"){
        console.log("TEST 2 FAILED: " + canBuild + ", missing: " + missing);
        passed = false;
      }else{
        //log("TEST 2 PASSED");
      }

      // TEST 3 - MISSING LIST IS CORRECT
      var missingList = simpleReport.needed;
      var expectedMissing = {
        "device": 5,
        "box": 0,
        "tape_cm": 0,
        "wrapped_pcb": 3,
        "pcb": 0,
        "ziploc": 0,
        "wrapped_hardware": 2,
        "screws": 4
      };
      if(equalObjs(missingList, expectedMissing)){
        //log("TEST 3 PASSED")
      }	else {
        console.log("TEST 3 FAILED");
        console.log(simpleReport);
        //console.log("&nbsp;&nbsp;&nbsp;&nbsp;Missing list:");
        //missingList.printStruct();
        //console.log("&nbsp;&nbsp;&nbsp;&nbsp;Expected list:");
        //expectedMissing.printStruct();
        passed = false;
      }

      // Data for test 4
      simpleDevice = {
        "root": {
          "screw": 2,
          "probes": 5
        },
        "probes": {
          "heatShrink": 0.1,
          "sensor": 1
        },
        "sensor": {
          "resistor": 2,
          "wire": 1
        }
      };

      simpleInventory = {
        "screw": 10,
        "probes": 15,
        "heatShrink": 8,
        "sensor": 2,
        "resistor": 1,
        "wire": 0
      };

      simplePO = {
        "screw": 0,
        "probes": 0,
        "heatShrink": 0,
        "sensor": 0,
        "resistor": 12,
        "wire": 5
      };

      simpleReport = generateReports("root", simpleDevice, simpleInventory, simplePO, 4, false);

      // TEST 4 - tests that non-integer quantities are handled correctly, and that upper nodes are correctly consumed first if in stock.
      // Also checks that PO calculations work.
      /*expected = {"success":false,"inventory":{"screw":10,"probes":15,"heatShrink":8,"sensor":2,"resistor":1,"wire":0},"missingItem":"resistor"};
      var expectedPO = {"success":true,"inventory":{"screw":2,"probes":0,"heatShrink":7.5,"sensor":0,"resistor":7,"wire":2},"missingItem":""};
      if(equalObjs(simpleReport.buildRemainder, expected) && equalObjs(simpleReport.buildRemainderPO, expectedPO)){
        //log("TEST 4 PASSED")
      }	else {
        logBold("TEST 4 FAILED");
        passed = false;
      }*/
      if(simpleReport.buildResults["root"].buildQty !== 3.4 || simpleReport.buildResultsPO["root"].buildQty !== 4.4){
        console.log("Build for test 4 not calculated properly. Should be 3.4 and 4.4:");
        console.log(simpleReport.buildResults.buildQty);
        console.log(simpleReport.buildResultsPO.buildQty);
        passed = false;
      }

      // Data for test 5
      simpleDevice = {
        "root": {
          "b": 1,
          "c": 1
        },
        "b": {
          "c": 1
        },
        "c": {
          "d": 1
        }
      };

      simpleInventory = {
        "b": 5,
        "c": 2,
        "d": 1
      };

      simplePO = {
        "b": 0,
        "c": 0,
        "d": 0
      };

      simpleReport = generateReports("root", simpleDevice, simpleInventory, simplePO, 2, true);
      expected = {"assemble":{"root":2,"b":2,"c":4},"buy":{"d":3}};
      var expected2 = {"root":{"b":1,"c":1},"b":{"c":1},"c":{"d":1}};

      // TEST 5 - check that the "assume zero for first level" inventory correctly works.
      if(equalObjs(simpleReport.exe, expected) && equalObjs(simpleReport.items, expected2)){
        //log("TEST 5 PASSED")
      }	else {
        console.log("TEST 5 FAILED");
        passed = false;
      }
      
      // Data for test 6
      simpleDevice = {
        "root": {
          "a": 1,
          "b": 1,
          "c": 1
        },
        "c": {
          "d": 1
        }
      };

      simpleInventory = {
        "a": 5,
        "b": 2,
        "c": 0,
        "d": 10,
      };

      simplePO = {
        "a": 0,
        "b": 2,
        "c": 0,
        "d": 0
      };

      simpleReport = generateReports("root", simpleDevice, simpleInventory, simplePO, 4, false);
      //expected = ;
      //var expected2 = ;

      // TEST 6 - this test highlights a specific case that resulted in a bug (overestimating build capabilities)
      if(simpleReport.buildResults["root"].buildQty !== 2 || simpleReport.buildResultsPO["root"].buildQty !== 4){
        console.log("TEST 6 failed. Report:");
        console.log(simpleReport);
        passed = false;
      }
      
      // TEST 7 - test the getDirectBuildCapability function
      simpleDevice = {
        "a": {
          "b": 1,
          "e_": 1
        },
        "b": {
          "c": 1,
          "d": 2
        },
        "c": {
          "g": 4
        },
        "e_":{
          "f": 0
        }
      };

      simpleInventory = {
        "a": 10,
        "b": 2,
        "c": 2,
        "d": 6,
        "e_": 3,
        "f": 50,
        "g": 475
      };
      
      simplePO = {
        "a": 0,
        "b": 0,
        "c": 0,
        "d": 0,
        "e_": 0,
        "f": 0,
        "g": 0
      };
      
      expected = {
        "a": 2,
        "b": 2,
        "c": 118.75,
        "d": 0,
        "e_": 0,
        "f": 0,
        "g": 0
      };
      expected2 = {
        "a":{"buildQty":3,"missingItem":"e_"},
        "b":{"buildQty":3,"missingItem":"d"},
        "c":{"buildQty":118.75,"missingItem":"g"},
        "d":{"buildQty":0,"missingItem":"d"},
        "e_":{"buildQty":0,"missingItem":"e_"},
        "f":{"buildQty":0,"missingItem":"f"},
        "g":{"buildQty":0,"missingItem":"g"}
      };
      
      simpleReport = generateReports("a", simpleDevice, simpleInventory, simplePO, 1, false);
      if(!equalObjs(simpleReport.canBuildNow, expected)){
        console.log("TEST 7 part 1 not passed");
        passed = false;
      }
      if(!equalObjs(simpleReport.buildResults,expected2)){
        console.log("TEST 7 part 2 not passed");
        passed = false;
        console.log("Expected:");
        console.log(expected2);
        console.log("Actual:");
        console.log(simpleReport.buildResults);
      }
      
      // TEST 8 - ensure we use the correct algorithm to get build capabilities (top-down recursive, not bottom up)
      simpleDevice = {
        "a": {
          "b": 1,
          "c": 1
        },
        "c": {
          "b": 1,
          "d": 1
        }
      };

      simpleInventory = {
        "a": 0,
        "b": 1000,
        "c": 0,
        "d": 1000
      };
      
      simplePO = {
        "a": 0,
        "b": 0,
        "c": 0,
        "d": 0
      };
      
      simpleReport = generateReports("a", simpleDevice, simpleInventory, simplePO, 1, false);
      if(simpleReport.buildResults["a"].buildQty != 500){
        console.log("TEST 8 not passed");
        console.log(simpleReport);
        passed = false;
      }
      
      // TEST 9 - bug with SubstractInventory?
      var items = {
        c: {g: 4}
      }
      
      var inventory = {
        c: 2,
        g: 475
      }
      
      var buildList = getFlatDeps(items, "c", 512);
      var needed = substractInventory(items, buildList, inventory, "c");
      if(!equalObjs(buildList,{c: 512, g: 2048})){
        console.log("TEST 9 part 1 not passed");
        console.log(buildList);
        passed = false;
      }
      if(!equalObjs(needed,{c: 512, g: 1573})){
        console.log("TEST 9 part 2 not passed");
        console.log(needed);
        passed = false;
      }
      
      // TEST 10 - Test the getPaths and getUsedBy functions
      var paths = getPaths({
        a: {b:1, c:2},
        c: {d:3, e:5},
        b: {c: 4}
      }, "a");
      var expected = [
        [{"a":1}, {"b":1}, {"c":4}, {"d":3}],
        [{"a":1}, {"b":1}, {"c":4}, {"e":5}],
        [{"a":1}, {"c":2}, {"d":3}],
        [{"a":1}, {"c":2}, {"e":5}]
      ];
      if(!equalObjs(paths,expected)){
        console.log("TEST 10 part 1 not passed");
        passed = false;
      }
      var usedBy = getUsedBy(paths, "e");
      expected = [
        [{"a":1}, {"b":1}, {"c":4}, {"e":5}],
        [{"a":1}, {"c":2}, {"e":5}]
      ];
      if(!equalObjs(usedBy,expected)){
        console.log("TEST 10 part 2 not passed");
        passed = false;
      }
      usedBy = getUsedBy(paths, "c");
      expected = [
        [{"a":1}, {"b":1}, {"c":4}],
        [{"a":1}, {"c":2}]
      ];
      if(!equalObjs(usedBy,expected)){
        console.log("TEST 10 part 3 not passed");
        passed = false;
      }
      
      // TEST 11 - when calculating the needed quantities, we should not assume we can "deconstruct" extra items to build others.
      simpleDevice = {
        "a": {
          "b": 1,
          "d": 1
        },
        "b": {
          "c": 6,
        },
        "d": {
          "c": 4
        }
      };

      simpleInventory = {
        "a": 0,
        "b": 8,
        "c": 10,
        "d": 0
      };
      
      simplePO = {
        "a": 0,
        "b": 0,
        "c": 0,
        "d": 0
      };
      
      expected = {a: 5, b: 0, c: 10, d: 5};
      
      simpleReport = generateReports("a", simpleDevice, simpleInventory, simplePO, 5, false);
      if(!equalObjs(simpleReport.needed,expected)){
        console.log("TEST 11 not passed");
        console.log(simpleReport);
        passed = false;
      }
      
      
      // TODO: test quantities are OK for Infinity items
      // TODO: test buildStatus is nonInventory
      // TEST 12 - items that have NON-INVENTORY in the name should be 'infinite' for the quantity
      /*simpleDevice = {
        "a": {
          "b": 1,
          "d": 1
        },
        "b": {
          "c": 6,
        },
        "d": {
          "c": 4
        }
      };

      simpleInventory = {
        "a": 0,
        "b": 8,
        "c": 10,
        "d": 0
      };
      
      simplePO = {
        "a": 0,
        "b": 0,
        "c": 0,
        "d": 0
      };
      
      nonInv = [];
      
      expected = {a: 5, b: 0, c: 10, d: 5};
      
      simpleReport = generateReports("a", simpleDevice, simpleInventory, simplePO, 5, false, nonInv);
      if(!equalObjs(simpleReport.needed,expected)){
        console.log("TEST 12 not passed");
        console.log(simpleReport);
        passed = false;
      }*/

      // Summary
      if(!passed){
        simpleReport.print();
        alert("Error. Self-test did not pass. Please contact support.");
      }
      return passed;
    };

    // Run self tests
    if(!testEngine()){
      var testError = "Internal tests did not pass! Please contact dominic.robillard@tytorobotics.ca";
      alert(testError);
    }else{
      console.log(data.design);
      results = generateReports(data.rootName, data.design, data.inventory, data.onPO, data.buildQty, assumeZero);
      results.testsPassed = true;
    }
    
    return results;
}