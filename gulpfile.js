var gulp  = require('gulp');
var minify = require('gulp-minify');

gulp.task('minify', ['copy'], function() {
  return gulp.src('src/*.js')
    .pipe(minify({
        ext:{
          min: ".js"
        },
        noSource: true
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('copy', function() {
  return gulp.src(['src/**/*', '!src/*.js'])
    .pipe(gulp.dest('dist'));
});
 
gulp.task('default', ['minify'], function() {});